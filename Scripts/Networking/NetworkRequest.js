import md5 from 'md5';
import {GetDateTimeString} from './DateTimeGetter';

const secretKey = '12345678';

export function performRequestData(apiUrl, parameterObject, callback) {
  let bodyData = {...parameterObject};
  let dateTime = GetDateTimeString();

  bodyData.dateTime = dateTime;

  let securityTokenString = '';

  for (let [key, value] of Object.entries(parameterObject)) {
    securityTokenString += value;
  }

  securityTokenString += dateTime;
  securityTokenString += secretKey;

  bodyData.securityToken = md5(securityTokenString);

  var formBody = [];
  for (var property in bodyData) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(bodyData[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');

  return fetch(apiUrl, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: formBody,
  })
    .then(response => response.json())
    .then(responseJson => {
      callback(responseJson);
    })
    .catch(error => {
      console.warn(error);
    });
}
