export function GetDateTimeString() {
  var dt = new Date();

  function formatNumber(targetNumber) {
    return ('0' + targetNumber).slice(-2);
  }

  return (
    dt.getFullYear() +
    '-' +
    formatNumber(dt.getMonth() + 1) +
    '-' +
    formatNumber(dt.getDate()) +
    ' ' +
    formatNumber(dt.getHours()) +
    ':' +
    formatNumber(dt.getMinutes()) +
    ':' +
    formatNumber(dt.getSeconds())
  );
}
