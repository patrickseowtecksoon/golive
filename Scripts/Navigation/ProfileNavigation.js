import React, {Component} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ProfileBaseScreen from '../MainScene/MainProfile/Scren';
import ProfileDetailScreen from '../MainScene/MainProfile/Detail/Screen';
import ProfileEventScreen from '../MainScene/MainProfile/Event/Screen';
import {View, StyleSheet} from 'react-native';
import {performRequestData} from '../Networking/NetworkRequest';

const Tab = createMaterialTopTabNavigator();

class ProfileNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelf: true,
      id: -1,
      name: '',
      age: 0,
    };

    let navParams = props.route.params;
    if (navParams === undefined) this.requestData(global.userToken);
    else {
      this.state.isSelf = false;
      this.requestData(navParams.id);
    }
  }

  requestData(userToken) {
    performRequestData(
      global.apiUrl + 'getUserDetails.shtml',
      {userToken: userToken},
      responseJson => {
        this.assignData(responseJson);
      },
    );
  }

  assignData(responseJson) {
    let userObj = JSON.parse(responseJson[2][2]);
    this.setState({name: userObj.name, age: userObj.age});
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <ProfileBaseScreen
          navigation={this.props.navigation}
          isSelf={this.state.isSelf}
          name={this.state.name}
          age={this.state.age}
        />
        <Tab.Navigator
          tabBarOptions={{
            showLabel: true,

            activeTintColor: '#278F9C',
            inactiveTintColor: '#666666',
            tabStyle: {
              marginTop: -7,
              height: 40,
              width: 100,
            },
            style: {
              backgroundColor: '#FFFFFF',
            },
            labelStyle: {
              fontSize: 15,
              alignSelf: 'center',
            },
            indicatorStyle: {
              height: '100%',
              backgroundColor: 'white',
              borderBottomWidth: 3,
              borderColor: '#278F9C',
            },
          }}>
          <Tab.Screen
            name="ProfileDetail"
            component={ProfileDetailScreen}
            options={{
              tabBarLabel: '簡介',
            }}
          />
          <Tab.Screen
            name="ProfileEvent"
            component={ProfileEventScreen}
            options={{
              tabBarLabel: '活動歷史',
            }}
          />
        </Tab.Navigator>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default ProfileNavigation;
