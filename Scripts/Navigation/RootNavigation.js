import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigation from './MainNavigation';
import LoginScreen from '../LoginScene/Screen';

const Stack = createStackNavigator();

const transitionConfig = {
  animation: 'timing',
  config: {
    duration: 0,
  },
};

class RootNavigation extends Component {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            headerShown: false,
            transitionSpec: {open: transitionConfig, close: transitionConfig},
          }}
        />
        <Stack.Screen
          name="Main"
          component={MainNavigation}
          options={{
            headerShown: false,
            transitionSpec: {open: transitionConfig, close: transitionConfig},
          }}
        />
      </Stack.Navigator>
    );
  }
}
export default RootNavigation;
