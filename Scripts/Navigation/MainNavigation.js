import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreenNavigation from './MainScreenNavigation';
import EventDetailScreen from '../MainScene/EventDetail/Screen';
import EventHostScreen from '../MainScene/EventHosting/Screen';
import ViewProfileNavigation from './ProfileNavigation';
import SettingScreen from '../MainScene/MainProfile/Setting/Screen';
import WebViewScreen from '../MainScene/WebView/Screen';
import PlacePickScreen from '../MainScene/GooglePlacesInput';

const Stack = createStackNavigator();

class RootNavigation extends Component {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="MainScreen"
          component={MainScreenNavigation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EventDetails"
          component={EventDetailScreen}
          options={{headerTitle: '活動詳情'}}
        />
        <Stack.Screen
          name="EventHost"
          component={EventHostScreen}
          options={{headerTitle: '創建活動'}}
        />
        <Stack.Screen
          name="ViewProfile"
          component={ViewProfileNavigation}
          options={{headerTitle: '瀏覽用戶頁面'}}
        />
        <Stack.Screen
          name="ProfileSetting"
          component={SettingScreen}
          options={{headerTitle: '主頁設定'}}
        />
        <Stack.Screen
          name="WebView"
          component={WebViewScreen}
          options={{headerTitle: '網頁'}}
        />
        <Stack.Screen
          name="PlacePick"
          component={PlacePickScreen}
          options={{headerTitle: '地點選擇'}}
        />
      </Stack.Navigator>
    );
  }
}
export default RootNavigation;
