import React, {Component} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import ProfileNavigation from './ProfileNavigation';
import EventMapScreen from '../MainScene/EventMap/Screen';
import EventPageNavigation from './EventPageNavigation';
import {createIconSetFromFontello} from 'react-native-vector-icons';
import fontelloConfig from '../../resources/fonts/config.json';
const CustomIcon = createIconSetFromFontello(fontelloConfig);

const Tab = createBottomTabNavigator();

class MainScreenNavigation extends Component {
  render() {
    return (
      <Tab.Navigator
        tabBarOptions={{
          swipeEnabled: true,
          animationEnabled: true,
          showIcon: true,
          showLabel: false,
          activeTintColor: '#28A19C',
          inactiveTintColor: '#666666',
          style: {
            backgroundColor: '#f5f5f5',
          },
        }}>
        <Tab.Screen
          name="MainProfile"
          component={ProfileNavigation}
          options={{
            tabBarIcon: ({color}) => (
              <Icon name="ios-contact" size={33} color={color} />
            ),
            tabBarLabel: '主頁',
          }}
        />
        <Tab.Screen
          name="EventMap"
          component={EventMapScreen}
          options={{
            tabBarIcon: ({color}) => (
              <CustomIcon name="golivelogo" size={33} color={color} />
            ),
            tabBarLabel: '發現',
          }}
        />
        <Tab.Screen
          name="EventPage"
          component={EventPageNavigation}
          options={{
            tabBarIcon: ({color}) => (
              <Icon name="ios-calendar" size={33} color={color} />
            ),
            tabBarLabel: '管理活動',
          }}
        />
      </Tab.Navigator>
    );
  }
}
export default MainScreenNavigation;
