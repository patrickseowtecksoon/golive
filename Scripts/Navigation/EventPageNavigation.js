import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MainEventPageScreen from '../MainScene/EventPage/MainEventPage/Screen';
import RequestedEventPageScreen from '../MainScene/EventPage/RequestedEventPage/Screen';

const Tab = createMaterialTopTabNavigator();

export default class EventPageNavigation extends Component {
  render() {
    return (
      <View style={styles.MainContainer}>
        <View style={styles.HeaderSection}>
          <Text style={styles.HeaderText}>我的活動</Text>
        </View>

        <Tab.Navigator
          tabBarOptions={{
            swipeEnabled: true,
            animationEnabled: true,
            showLabel: true,
            activeTintColor: '#278F9C',
            inactiveTintColor: '#666666',
            tabStyle: {
              marginTop: -7,
              height: 40,
              width: 100,
            },
            style: {
              backgroundColor: '#FFFFFF',
            },
            labelStyle: {
              fontSize: 15,
              alignSelf: 'center',
            },
            indicatorStyle: {
              height: '100%',
              backgroundColor: 'white',
              borderBottomWidth: 3,
              borderColor: '#278F9C',
            },
          }}>
          <Tab.Screen
            name="MainEvent"
            component={MainEventPageScreen}
            options={{
              tabBarLabel: '活動管理',
            }}
          />
          <Tab.Screen
            name="RequestedEvent"
            component={RequestedEventPageScreen}
            options={{
              tabBarLabel: '已申請',
            }}
          />
        </Tab.Navigator>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  HeaderSection: {
    borderWidth: 10,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  HeaderText: {
    fontSize: 30,
    fontWeight: '500',
  },
});
