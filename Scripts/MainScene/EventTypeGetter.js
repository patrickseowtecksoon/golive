import React, {Component} from 'react';

const availableEvents = [
  'Others',
  'MeetNewFriends',
  'Dating',
  'Sports',
  'Entertainments',
  'Discussions',
  'Business',
  'Dining',
];

export function getEventType(intIndex) {
  let resultIndex = 0;

  if (intIndex < availableEvents.length) resultIndex = intIndex;

  return availableEvents[resultIndex];
}

export function getEventString(eventType) {
  if (eventType == 'MeetNewFriends') {
    return '交友';
  } else if (eventType === 'Dating') {
    return '約會';
  } else if (eventType === 'Sports') {
    return '運動';
  } else if (eventType === 'Entertainments') {
    return '娛樂';
  } else if (eventType === 'Discussions') {
    return '交談';
  } else if (eventType === 'Business') {
    return '生意';
  } else if (eventType === 'Dining') {
    return '進餐';
  } else {
    return '其他';
  }
}

export function getAvailableEvents() {
  return availableEvents;
}
