import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import EventItem from '../../DetailEventItemGetter';

class EventScreen extends Component {
  render() {
    const navigationRef = this.props.navigation;
    return (
      <View style={styles.MainContainer}>
        <View style={styles.GeneralContent}>
          <View style={styles.GeneralHeaderContent}>
            <Text>Featured</Text>
            <Icon name="star" size={20} color={'#FEE12B'} />
          </View>

          <FlatList
            data={this.getContentsData}
            style={styles.ContentListContainer}
            renderItem={item => <RenderContent itemParams={item} />}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );

    function RenderContent(itemParams) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          style={styles.ContentItem}
          onPress={() => {
            navigationRef.navigate('EventDetails', itemParams.itemParams.item);
          }}>
          <EventItem itemData={itemParams} />
        </TouchableOpacity>
      );
    }
  }

  get getContentsData() {
    const data = [
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        imgLink: {
          uri:
            'https://www.newsbugz.com/wp-content/uploads/2019/04/best-party-planner.jpg',
        },
        title: 'Lets Rock the Night',
        time: '8th Sep, 3:00pm',
        location: 'Zouk, Kuala Lumpur',
      },
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bb',
        imgLink: {
          uri:
            'https://assets.nst.com.my/images/articles/Joo_Ven_1550923383.jpg',
        },
        title: 'Jump and Smash',
        time: '8th Sep, 3:00pm',
        location: 'Sports Arena, Kuala Lumpur',
      },
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bc',
        imgLink: {
          uri:
            'https://cnet1.cbsistatic.com/img/bM6_8sk2-FFgxrr4E9jgyJLyeNY=/644x0/2019/08/26/84874730-13f8-42e9-8885-3c23c08c358c/gettyimages-1127551032.jpg',
        },
        title: 'Reach to the Sky',
        time: '8th Sep, 3:00pm',
        location: 'Bukit Bintang, Selangor',
      },
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28be',
        imgLink: {
          uri:
            'https://fsgw.org/resources/Programs/ECD%20Spring%20Ball/spring%20ball%20carousel%20pics/ECD%20Ball%201.jpg',
        },
        title: 'Dacing in the Dark',
        time: '8th Sep, 3:00pm',
        location: 'Marcopolo, Kuala Lumpur',
      },
    ];
    return data;
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  GeneralContent: {
    flex: 1,
    borderColor: 'rgba(0, 0, 0, 0)',
    borderLeftWidth: 10,
    borderTopWidth: 7,
    borderBottomWidth: 4,
  },
  GeneralHeaderContent: {
    flex: 0.2,
    flexDirection: 'row',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  HeaderText: {
    flex: 1,
    flexDirection: 'column',
  },
  ContentListContainer: {},
  ContentItem: {
    paddingBottom: 5,
  },
});

export default EventScreen;
