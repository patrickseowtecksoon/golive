import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FIcon from 'react-native-vector-icons/FontAwesome';

export default class EventTypeIconGetter extends Component {
  render() {
    return <View style={styles.MainContainer}>{this.getIcon}</View>;
  }

  get getIcon() {
    const results = [];
    const iconColor =
      this.props.iconColor == undefined ? '#278F9C' : this.props.iconColor;
    let eventLabel = '';

    if (this.props.eventType == 'MeetNewFriends') {
      results.push(
        <Icon
          name="account-multiple"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '交友';
    } else if (this.props.eventType === 'Dating') {
      results.push(
        <Icon
          name="heart"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '約會';
    } else if (this.props.eventType === 'Sports') {
      results.push(
        <Icon
          name="soccer"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '運動';
    } else if (this.props.eventType === 'Entertainments') {
      results.push(
        <Icon
          name="controller-classic"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '娛樂';
    } else if (this.props.eventType === 'Discussions') {
      results.push(
        <Icon
          name="chat-processing"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '交談';
    } else if (this.props.eventType === 'Business') {
      results.push(
        <FIcon
          name="black-tie"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '生意';
    } else if (this.props.eventType === 'Dining') {
      results.push(
        <Icon
          name="silverware-fork-knife"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '進餐';
    } else if (this.props.eventType === 'Others') {
      results.push(
        <Icon
          name="dots-horizontal"
          size={this.props.size}
          color={iconColor}
          style={this.props.iconStyle}
        />,
      );
      eventLabel = '其他';
    }

    if (this.props.showLabel) {
      results.push(<Text style={this.props.labelStyle}>{eventLabel}</Text>);
    }

    return results;
  }
}

const styles = StyleSheet.create({
  MainContainer: {flexDirection: 'column'},
});
