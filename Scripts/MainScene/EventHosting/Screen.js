import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  Picker,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import MIIcon from 'react-native-vector-icons/MaterialIcons';
import {getEventString} from '../EventTypeGetter';
import MultipleImagePicker from '../Utils/ImagePicker/MultipleImagePicker';
import EventTimePicker from './EventTimePicker';
import {performRequestData} from '../../Networking/NetworkRequest';

class EventHostScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventTitle: undefined,
      selectedEventType: global.availableEvents[0],
      descValue: '',
      limitJoiner: '',
      startDate: '',
      endDate: '',
      locationData: undefined,
    };
  }

  requestData(targetStatus) {
    performRequestData(
      global.apiUrl + 'createEvent.shtml',
      {
        title: this.state.eventTitle,
        description: this.state.descValue,
        category: '',
        startDate: '',
        endDate: '',
        l1: '',
        l2: '',
        type: '',
        address: '',
        addPostcode: '',
        addState: '',
        dateTime: '',
        securityToken: '',
      },
      responseJson => {
        this.assignData(responseJson, targetStatus);
      },
    );
  }

  handleLimitJoinerInputChange = text => {
    if (/^\d+$/.test(text)) {
      this.setState({
        limitJoiner: text,
      });
    }
  };

  render() {
    const navigation = this.props.navigation;
    //#region event type
    function setEventTypePicker() {
      const result = [];
      for (let i = 0; i < global.availableEvents.length; i++) {
        result.push(
          <Picker.Item
            label={getEventString(global.availableEvents[i])}
            value={global.availableEvents[i]}
          />,
        );
      }

      return result;
    }

    function renderLocationText(stateData) {
      if (stateData.locationData === undefined) {
        return <Text style={styles.LocationTextDisabled}>選擇地點</Text>;
      } else {
        return (
          <Text style={styles.LocationText}>
            {stateData.locationData.addressLevel1 +
              ', ' +
              stateData.locationData.addressLevel2}
          </Text>
        );
      }
    }

    //#endregion

    return (
      <View>
        <ScrollView style={styles.MainContainer}>
          <Text style={styles.OptionText}>標題*</Text>
          <TextInput
            style={styles.EventTitleTextInput}
            onChangeText={text => this.setState({eventTitle: text})}
            placeholder={'活動標題'}
            value={this.state.eventTitle}
          />
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>
          <Text style={styles.OptionText}>活動種類*</Text>
          <Picker
            selectedValue={this.state.selectedEventType}
            style={styles.EventTypePicker}
            mode="dropdown"
            onValueChange={(itemValue, itemIndex) =>
              this.setState({selectedEventType: itemValue})
            }>
            {setEventTypePicker()}
          </Picker>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>
          <Text style={styles.OptionText}>封面圖*</Text>
          <View style={{height: 5}}></View>
          <MultipleImagePicker maxCount={1} />
          <View style={{height: 7}}></View>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>
          <Text style={styles.OptionText}>地點*</Text>
          <View style={{height: 7}}></View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('PlacePick', {
                locationAcquiredCallback: locationData => {
                  this.setState({locationData: locationData});
                },
              });
            }}>
            <Text>{renderLocationText(this.state)}</Text>
          </TouchableOpacity>
          <View style={{height: 7}}></View>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>
          <Text style={styles.OptionText}>時間*</Text>
          <View style={{height: 6}}></View>
          <EventTimePicker titleLabel="開始" />
          <View style={{height: 6}}></View>
          <EventTimePicker titleLabel="結束" />
          <View style={{height: 7}}></View>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>

          <Text style={styles.OptionText}>人數限制</Text>
          <TextInput
            style={styles.EventTitleTextInput}
            onChangeText={this.handleLimitJoinerInputChange}
            placeholder={'沒限制'}
            value={this.state.limitJoiner}
            keyboardType="numeric"
            maxLength={4}
          />
          <View style={{height: 7}}></View>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>
          <Text style={styles.OptionText}>概述</Text>
          <View style={styles.DescriptionTextContainer}>
            <TextInput
              multiline={true}
              numberOfLines={3}
              maxLength={130}
              placeholder="活動簡介。。。"
              value={this.state.descValue}
              onChangeText={value => this.setState({descValue: value})}
              style={styles.DescriptionTextField}
            />
            <Text style={styles.DescriptionRemainingText}>
              {this.state.descValue.length}/130
            </Text>
          </View>

          <View style={{height: 7}}></View>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>

          <Text style={styles.OptionText}>圖冊</Text>
          <View style={{height: 5}}></View>
          <MultipleImagePicker maxCount={4} />
          <View style={{height: 7}}></View>
          <View style={styles.Devider} />
          <View style={{height: 20}}></View>

          <TouchableOpacity
            style={styles.ConfirmButton}
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('MainScreen', {
                screen: 'EventPage',
                params: {screen: 'MainEvent'},
              });
            }}>
            <Text style={styles.ConfirmButtonText}>確認</Text>
          </TouchableOpacity>
          <View style={{height: 20}}></View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    borderWidth: 10,
    borderColor: 'rgba(0, 0, 0, 0)',
  },
  Devider: {
    height: 2,
    backgroundColor: '#c9c9c9',
  },
  OptionText: {
    fontSize: 15,
    color: '#278F9C',
  },
  EventTitleTextInput: {
    height: 35,

    padding: 3,
  },
  EventTypePicker: {
    width: 120,
    height: 30,
  },
  ConfirmButton: {
    width: 100,
    height: 35,
    borderRadius: 10,
    backgroundColor: '#278F9C',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  ConfirmButtonText: {
    fontSize: 15,
    alignSelf: 'center',
    color: 'white',
  },
  DescriptionTextContainer: {
    borderColor: 'black',
    borderWidth: 1,
  },
  DescriptionTextField: {
    textAlignVertical: 'top',
  },
  DescriptionRemainingText: {
    alignSelf: 'flex-end',
    padding: 3,
    color: '#c9c9c9',
  },
  LocationTextDisabled: {
    fontSize: 15,
    color: '#b8b8b8',
  },
  LocationText: {
    fontSize: 15,
  },
});

export default EventHostScreen;
