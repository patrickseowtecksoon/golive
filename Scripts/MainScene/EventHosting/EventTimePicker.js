import React, {Component, useState} from 'react';
import {
  View,
  Button,
  Platform,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Moment from 'moment';

class EventTimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentDate: new Date().getDate(),
      isShow: false,
      dateCaptured: false,
      timeCaptured: false,
    };
  }

  mode = undefined;

  Shows(mode) {
    this.mode = mode;
    this.setState({isShow: true});
  }

  OnChange(mode, selectedDate) {
    this.setState({isShow: false});
    if (selectedDate == undefined) return;

    if (mode == 'date') this.setState({dateCaptured: true});
    else if (mode == 'time') this.setState({timeCaptured: true});

    this.setState({currentDate: selectedDate});
  }

  render() {
    function doDateLabelSection(isCaptured, dateTime) {
      if (!isCaptured)
        return <Text style={styles.SegmentLabel}>按此輸入日期</Text>;
      else
        return (
          <Text style={styles.SegmentLabel}>
            {Moment(dateTime).format('DD MMM YYYY')}
          </Text>
        );
    }

    function doTimeLabelSection(isCaptured, dateTime) {
      if (!isCaptured)
        return <Text style={styles.SegmentLabel}>按此輸入時間</Text>;
      else
        return (
          <Text style={styles.SegmentLabel}>
            {Moment(dateTime).format('LT')}
          </Text>
        );
    }

    return (
      <View style={styles.MainContainer}>
        <View style={styles.OptionItem}>
          <Text style={styles.OptionText}>{this.props.titleLabel}</Text>

          <View style={styles.SegmentSection}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.Shows('date');
              }}>
              <View style={styles.SegmentItem}>
                <View style={styles.SegmentLabelSection}>
                  {doDateLabelSection(
                    this.state.dateCaptured,
                    this.state.currentDate,
                  )}
                </View>
                <View style={styles.SegmentIconSection}>
                  <Icon
                    style={styles.SegmentIcon}
                    name="calendar"
                    size={20}
                    color="#278F9C"
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>

            <View style={styles.SegmentDevider}></View>
            <TouchableWithoutFeedback
              onPress={() => {
                this.Shows('time');
              }}>
              <View style={styles.SegmentItem}>
                <View style={styles.SegmentLabelSection}>
                  {doTimeLabelSection(
                    this.state.timeCaptured,
                    this.state.currentDate,
                  )}
                </View>
                <View style={styles.SegmentIconSection}>
                  <Icon
                    style={styles.SegmentIcon}
                    name="clock-outline"
                    size={20}
                    color="#278F9C"
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        {this.state.isShow && (
          <DateTimePicker
            timeZoneOffsetInMinutes={0}
            value={this.state.currentDate}
            mode={this.mode}
            is24Hour={false}
            display="default"
            onChange={(event, selectedDate) => {
              this.OnChange(this.mode, selectedDate);
            }}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    width: '100%',
  },
  OptionText: {
    fontSize: 14,
  },
  OptionItem: {
    width: '100%',
  },
  SegmentSection: {flexDirection: 'row'},
  SegmentItem: {
    flexDirection: 'row',
    width: '40%',
    borderBottomWidth: 1,
    borderColor: '#c9c9c9',
  },
  SegmentDevider: {
    width: '20%',
  },
  SegmentLabelSection: {width: '70%'},
  SegmentLabel: {fontSize: 13, width: 90, color: '#b8b8b8'},
  SegmentIconSection: {flexDirection: 'column', width: '30%'},
  SegmentIcon: {alignSelf: 'flex-end'},
});

export default EventTimePicker;
