import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  Dimensions,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PopUpContent from './PopUpContent';

class EventFilterPopUp extends Component {
  state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View style={styles.contentContainer}>
        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}
          style={styles.touchableHighlight}
          underlayColor={'#f1f1f1'}>
          <Icon name="filter-list" size={30} style={styles.buttonIcon} />
        </TouchableHighlight>

        <Modal
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(false);
          }}
          animationType="fade"
          transparent={true}>
          <PopUpContent
            changeModalVisibility={this.setModalVisible.bind(this)}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  touchableHighlight: {
    backgroundColor: 'white',
    alignItems: 'stretch',
    alignItems: 'center',
  },
  buttonIcon: {
    color: '#278F9C',
  },
});

export default EventFilterPopUp;
