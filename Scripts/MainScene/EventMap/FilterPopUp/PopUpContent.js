import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  FlatList,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconGetter from '../../EventTypeIconGetter';

class PopUpContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenWidth: Dimensions.get('window').width,
      screnHeight: Dimensions.get('window').height,
      selectedSortingIndex: 0,
      filterEventData: [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          eventType: 'MeetNewFriends',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bb',
          eventType: 'Dating',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bc',
          eventType: 'Sports',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bd',
          eventType: 'Entertainments',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28be',
          eventType: 'Discussions',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bf',
          eventType: 'Business',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bf',
          eventType: 'Dining',
          selected: true,
        },
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bf',
          eventType: 'Others',
          selected: true,
        },
      ],
    };

    Dimensions.addEventListener('change', e => {
      this.setState(e.window);
    });
  }

  closeModal() {
    this.props.changeModalVisibility(false);
  }

  //#region sorting

  onSortingOptionSelected(index) {
    this.selectedSortingIndex = index;
    this.setState(
      prevState => ({selectedSortingIndex: index}),
      () => {},
    );
  }

  getSortingOptionRenderIcon(selfIndex) {
    if (selfIndex == this.state.selectedSortingIndex)
      return (
        <Icon
          name="radio-button-checked"
          size={20}
          style={styles.CheckerIcon}
        />
      );
    else
      return (
        <Icon
          name="radio-button-unchecked"
          size={20}
          style={styles.CheckerIcon}
        />
      );
  }

  //#endregion

  //#region  filter

  setFilterOptionSelected(index) {
    let newList = this.state.filterEventData;
    newList[index].selected = !newList[index].selected;

    this.setState(
      prevState => ({filterEventData: newList}),
      () => {},
    );
  }

  //#endregion

  render() {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          this.props.changeModalVisibility(false);
        }}
        style={styles.ContentContainer}>
        <TouchableOpacity activeOpacity={1}>
          <View style={[styles.Modal]}>
            <View style={styles.SortingSection}>
              <Text style={styles.HeaderText}>排列</Text>
              <View style={styles.Devider}></View>
              <View style={{height: 5}}></View>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.onSortingOptionSelected(0);
                }}>
                <View style={styles.Checker}>
                  {this.getSortingOptionRenderIcon(0)}
                  <View style={{width: 5}} />
                  <Text style={styles.CheckerText}>開始時間</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.onSortingOptionSelected(1);
                }}>
                <View style={styles.Checker}>
                  {this.getSortingOptionRenderIcon(1)}
                  <View style={{width: 5}} />
                  <Text style={styles.CheckerText}>地點距離</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={{height: 5}} />
            <View style={styles.FilterSection}>
              <Text style={styles.HeaderText}>篩選</Text>
              <View style={styles.Devider}></View>
              <FlatList
                data={this.getAvailableFilterData}
                style={styles.FilterFlatList}
                numColumns={5}
                renderItem={({item, index, separators}) => (
                  <View
                    style={[
                      styles.FilterItem,
                      {opacity: item.selected ? 1 : 0.25},
                    ]}>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        {
                          this.setFilterOptionSelected(index);
                        }
                      }}>
                      <View>
                        <IconGetter
                          eventType={item.eventType}
                          size={30}
                          showLabel={true}
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                )}
                keyExtractor={item => item.id}
              />
            </View>
            <View style={{height: 5}} />
            <TouchableOpacity
              onPress={() => {
                this.props.changeModalVisibility(false);
              }}>
              <View style={styles.CloseButton}>
                <Text style={styles.CloseButtonText}>確認</Text>
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

  get getAvailableFilterData() {
    return this.state.filterEventData;
  }
}

const styles = StyleSheet.create({
  ContentContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  Modal: {
    borderColor: 'white',
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 15,
  },
  HeaderText: {
    fontSize: 17,
  },
  CheckBoxTextStyle: {
    fontSize: 17,
    alignSelf: 'center',
    textAlign: 'center',
  },
  Devider: {
    width: 230,
    height: 1,
    backgroundColor: 'gray',
  },
  Checker: {
    width: 200,
    height: 30,
    flexDirection: 'row',
  },
  CheckerIcon: {},
  CheckerText: {
    fontSize: 15,
  },
  SortingSection: {
    height: 100,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: 'gray',
  },
  FilterSection: {
    height: 170,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: 'gray',
  },
  FilterFlatList: {},
  FilterItem: {
    padding: 7,
  },
  CloseButton: {
    width: 100,
    height: 35,
    borderRadius: 10,
    backgroundColor: '#28A19C',
    alignItems: 'center',
    justifyContent: 'center',
  },
  CloseButtonText: {
    fontSize: 15,
    alignSelf: 'center',
    color: 'white',
  },
});

export default PopUpContent;
