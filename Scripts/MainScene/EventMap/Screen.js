import React, {Component} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import EventItem from '../DetailEventItemGetter';
import FilterPopUp from './FilterPopUp/EventFilterPopUp';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RNLocation from 'react-native-location';
import {getEventType} from '../EventTypeGetter';
import {performRequestData} from '../../Networking/NetworkRequest';

const screenHeight = Math.round(Dimensions.get('window').height);

class EventMapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventData: [],
    };
    this.fetchUserLocation();
  }

  fetchUserLocation() {
    RNLocation.configure({
      distanceFilter: 5.0,
    });

    RNLocation.requestPermission({
      ios: 'whenInUse',
      android: {
        detail: 'coarse',
      },
    }).then(granted => {
      if (granted) {
        this.locationSubscription = RNLocation.subscribeToLocationUpdates(
          locations => {
            this.requestData(locations[locations.length - 1]);
          },
        );
      }
    });
  }

  requestData(locationData) {
    let l1 = locationData.longitude;
    let l2 = locationData.latitude;

    performRequestData(
      global.apiUrl + 'getActiveEvent.shtml',
      {l1: locationData.longitude, l2: locationData.latitude},
      responseJson => {
        this.assignData(responseJson);
      },
    );
  }

  assignData(responseJson) {
    let eventData = [];

    for (let i = 0; i < responseJson[2].length; i++) {
      eventData.push({
        id: responseJson[2][i][3],
        //imgLink: responseJson[2][i][6],
        imgLink:
          'https://d3hne3c382ip58.cloudfront.net/files/uploads/bookmundi/resized/cmsfeatured/hiking-trails-indonesia-1523345692-785X440.jpg',
        hostInfo: {
          name: 'ChunKit',
          imgLink:
            'https://image.shutterstock.com/image-photo/handsome-fit-young-man-countryside-260nw-276450281.jpg',
        },
        title: responseJson[2][i][0],
        eventType: getEventType([responseJson[2][i][2]]),
      });
    }

    this.setState({eventData: eventData});
  }

  render() {
    const navigationRef = this.props.navigation;
    return (
      <View>
        <View style={styles.MainContainer}>
          <View style={styles.HeaderSection}>
            <View style={styles.HeaderTitleSection}>
              <Text style={styles.TitleText}>發現活動</Text>
            </View>
            <View style={styles.HeaderIconSection}>
              <View style={styles.HeaderIconContainer}>
                <TouchableOpacity
                  onPress={() => {
                    navigationRef.navigate('EventHost');
                  }}>
                  <Icon name="add" size={30} style={styles.HeaderIcon} />
                </TouchableOpacity>

                <View style={{width: 10}} />
                <View style={styles.HeaderIcon}>
                  <FilterPopUp />
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              height: 0.7,
              backgroundColor: 'rgba(0,0,0,0.25)',
            }}
          />
          <View
            style={{
              height: 0.7,
              backgroundColor: 'rgba(0,0,0,0.20)',
            }}
          />
          <View
            style={{
              height: 0.7,
              backgroundColor: 'rgba(0,0,0,0.15)',
            }}
          />
          <View
            style={{
              height: 0.7,
              backgroundColor: 'rgba(0,0,0,0.1)',
            }}
          />
          <View
            style={{
              height: 0.7,
              backgroundColor: 'rgba(0,0,0,0.05)',
            }}
          />

          <FlatList
            data={this.state.eventData}
            style={styles.ContentListContainer}
            renderItem={item => <RenderContent itemParams={item} />}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );

    function RenderContent(itemParams) {
      return (
        <TouchableHighlight
          style={styles.ContentItem}
          onPress={() => {
            navigationRef.navigate('EventDetails', itemParams.itemParams.item);
          }}>
          <EventItem itemData={itemParams} />
        </TouchableHighlight>
      );
    }
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    borderColor: 'white',
    height: '100%',
  },
  HeaderSection: {
    flexDirection: 'row',
    height: 66,
    backgroundColor: 'white',
    borderWidth: 10,
    borderColor: 'white',
  },
  HeaderTitleSection: {
    flex: 0.5,
  },
  TitleText: {
    fontSize: 30,
    fontWeight: '500',
  },
  HeaderIconSection: {
    flexDirection: 'column',
    flex: 0.5,
    height: '100%',
  },
  HeaderIconContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    height: '100%',
  },
  HeaderIcon: {
    alignSelf: 'center',
    color: '#278F9C',
  },
  ContentListContainer: {},
});

export default EventMapScreen;
