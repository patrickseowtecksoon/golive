import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet, Text, Image} from 'react-native';
import MIIcon from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';

const imagePickerOptions = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class MultipleImagePicker extends Component {
  constructor(props) {
    super(props);
    this.countUpdatedCallback = this.props.countUpdatedCallback;
    this.state = {
      avatarSource: [],
    };
  }

  CaptureCoverPhoto() {
    ImagePicker.showImagePicker(imagePickerOptions, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.AddIcon(this.state.avatarSource, source);
      }
    });
  }

  AddIcon(originalSource, targetSource) {
    let sourceHolder = [];

    for (let i = 0; i < originalSource.length; i++) {
      sourceHolder.push(originalSource[i]);
    }

    sourceHolder.push(targetSource);

    this.setState({
      avatarSource: sourceHolder,
    });

    if (this.countUpdatedCallback != undefined)
      this.countUpdatedCallback(sourceHolder.length);
  }

  ClearIcon(targetIndex) {
    let result = [];

    for (let i = 0; i < this.state.avatarSource.length; i++) {
      if (i != targetIndex) result.push(this.state.avatarSource[i]);
    }

    this.setState({avatarSource: result});

    if (this.countUpdatedCallback != undefined)
      this.countUpdatedCallback(result.length);
  }

  RenderIcons(source, maxCount) {
    const result = [];
    for (let i = 0; i < source.length; i++) {
      result.push(
        <View style={styles.IconContainer} borderStyle="dashed">
          <Image source={source[i]} style={styles.AddedIcon} />
          <View style={styles.CloseButtonSection}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                this.ClearIcon(i);
              }}>
              <View style={styles.CloseButtonContainer}>
                <MIIcon style={styles.CloseButton} name="close" size={20} />
              </View>
            </TouchableOpacity>
          </View>
        </View>,
      );
      result.push(<View style={{width: 2}}></View>);
    }

    if (result.length / 2 < maxCount) {
      result.push(
        <TouchableOpacity
          onPress={() => {
            this.CaptureCoverPhoto();
          }}
          activeOpacity={0.5}>
          <View style={styles.IconContainer} borderStyle="dashed">
            <MIIcon name="add-a-photo" size={35} style={styles.AddIcon} />
          </View>
        </TouchableOpacity>,
      );
    }

    return result;
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        {this.RenderIcons(this.state.avatarSource, this.props.maxCount)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flexDirection: 'row',
  },
  IconContainer: {
    width: 80,
    height: 80,
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',

    borderColor: '#8c8c8c',
  },
  AddIcon: {color: '#8c8c8c'},
  AddedIcon: {width: '100%', height: '100%'},
  CloseButtonSection: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  CloseButtonContainer: {
    alignSelf: 'flex-end',

    borderWidth: 3,
    width: 25,
    height: 25,
    borderColor: '#28A19C',
    borderRadius: 2,
    backgroundColor: 'rgba(125, 125, 125, 0.8)',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  CloseButton: {
    alignSelf: 'center',
    color: '#002627',
  },
});

export default MultipleImagePicker;
