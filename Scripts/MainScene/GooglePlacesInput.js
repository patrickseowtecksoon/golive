import React, {Component} from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';

class GooglePlacesInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      targetCoordinate: {
        latitude: 3.1466,
        longitude: 101.6958,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
      locationData: undefined,
    };
  }

  storeTargetCoordinate(parameter) {
    let result = {
      latitude: parameter.location.lat,
      longitude: parameter.location.lng,
      latitudeDelta: 0,
      longitudeDelta: 0,
    };
    this.setState({
      targetCoordinate: result,
    });
  }

  storeLocationData(details) {
    let result = {
      addressLevel1: details[0].value,
      addressLevel2: details[1].value,
    };

    this.setState({locationData: result});
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <View style={styles.MapSection}>
          <MapView
            provider={PROVIDER_GOOGLE}
            region={this.state.targetCoordinate}
            style={styles.Map}
            rotateEnabled={false}
            pitchEnabled={false}
            scrollEnabled={false}
            minZoomLevel={14}
            maxZoomLevel={14}></MapView>
        </View>
        <View style={styles.SearchBarSection}>
          <GooglePlacesAutocomplete
            placeholder="Search"
            minLength={2} // minimum length of text to search
            autoFocus={false}
            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
            keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
            listViewDisplayed="false" // true/false/undefined
            fetchDetails={true}
            renderDescription={row => row.description} // custom description render
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              this.storeLocationData(data.terms);
              this.storeTargetCoordinate(details.geometry);
            }}
            getDefaultValue={() => ''}
            query={{
              // available options: https://developers.google.com/places/web-service/autocomplete
              key: 'AIzaSyBTjiUpOekQIngfG3fjUKmTnj7ZnbgblZ8',
              language: 'en', // language of the results
              types: this.props.route.params.searchLevel, // default: 'geocode'
              components: 'country:my',
            }}
            styles={{
              textInputContainer: {
                width: '100%',
              },
              description: {
                fontWeight: 'bold',
              },
              container: {
                backgroundColor: 'white',
              },
              predefinedPlacesDescription: {
                color: '#1faadb',
              },
            }}
            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
            currentLocationLabel="Current location"
            nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
            GoogleReverseGeocodingQuery={
              {
                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
              }
            }
            GooglePlacesSearchQuery={{
              // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
              rankby: 'distance',
              type: 'cafe',
            }}
            GooglePlacesDetailsQuery={
              {
                // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                //fields: 'address_component,geometry',
              }
            }
            filterReverseGeocodingByTypes={[
              'locality',
              'administrative_area_level_3',
            ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            renderRightButton={() => (
              <View style={styles.CheckButtonSection}>
                <TouchableOpacity
                  onPress={() => {
                    if (
                      this.state.targetCoordinate != undefined &&
                      this.props.route.params.locationAcquiredCallback !=
                        undefined
                    ) {
                      this.props.route.params.locationAcquiredCallback(
                        this.state.locationData,
                      );
                      this.props.navigation.goBack();
                    }
                  }}>
                  <View style={styles.CheckButton}>
                    <Icon
                      name="check"
                      size={15}
                      color={'#002627'}
                      style={styles.CheckButtonIcon}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {flex: 1},
  SearchBarSection: {position: 'absolute', width: '100%'},
  MapSection: {flex: 1},
  Map: {height: '100%'},
  CheckButtonSection: {
    alignSelf: 'center',
    width: 37,
    height: 32,
    flexDirection: 'row',
  },
  CheckButton: {
    borderWidth: 1.7,
    borderRadius: 7,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignSelf: 'center',
    borderColor: '#28A19C',
  },
  CheckButtonIcon: {
    alignSelf: 'center',
  },
});
export default GooglePlacesInput;
