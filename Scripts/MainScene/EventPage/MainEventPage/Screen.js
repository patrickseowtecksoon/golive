import React, {Component} from 'react';
import {
  View,
  UIManager,
  StyleSheet,
  ScrollView,
  LayoutAnimation,
} from 'react-native';
import ExpandableItemComponent from '../ExpandableItemComponent';
import {getEventType} from '../../EventTypeGetter';
import {performRequestData} from '../../../Networking/NetworkRequest';

class MainEventPageScreen extends Component {
  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      onGoingEventData: [],
      upComingEventData: [],
      pastEventData: [],
      listDataSource: CONTENT,
    };
    this.requestData();
  }

  requestData() {
    performRequestData(
      global.apiUrl + 'getOwnEvent.shtml',
      {},
      responseJson => {
        this.assignData(responseJson);
      },
    );
  }

  assignData(responseJson) {
    let onGoingEventData = [];
    let upComingEventData = [];
    let pastEventData = [];
    let eventRawData = responseJson[2];

    for (let i = 0; i < eventRawData.length; i++) {
      let eventData = {
        id: eventRawData[i][3],
        eventType: getEventType([eventRawData[i][2]]),
        title: eventRawData[i][0],
        //startTime: eventRawData[i][8],
        startTime: '8th Sep, 3:00pm',
        //address: eventRawData[i][9],
        address: 'Pavilion',
        //stateAddress: eventRawData[i][10],
        stateAddress: 'Kuala Lumpur',
      };

      let status = eventRawData[i][7];

      if (status === 10) onGoingEventData.push(eventData);
      else if (status === 20) upComingEventData.push(eventData);
      else pastEventData.push(eventData);
    }

    let listDataSource = this.state.listDataSource;
    listDataSource[0].contents = onGoingEventData;
    listDataSource[1].contents = upComingEventData;
    listDataSource[2].contents = pastEventData;

    this.setState({
      onGoingEventData: onGoingEventData,
      upComingEventData: upComingEventData,
      pastEventData: pastEventData,
      listDataSource: listDataSource,
    });
  }

  updateLayout = index => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...this.state.listDataSource];

    array[index]['isExpanded'] = !array[index]['isExpanded'];

    this.setState(() => {
      return {
        listDataSource: array,
      };
    });
  };

  onItemClicked(item) {
    this.props.navigation.navigate('EventDetails', item);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.state.listDataSource.map((item, key) => (
            <ExpandableItemComponent
              key={item.category_name}
              onClickFunction={this.updateLayout.bind(this, key)}
              onClickItemFunction={this.onItemClicked.bind(this)}
              item={item}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});

const CONTENT = [
  {
    isExpanded: true,
    category_name: '進行中',
    contents: [],
  },
  {
    isExpanded: true,
    category_name: '即將來臨',
    contents: [],
  },
  {
    isExpanded: false,
    category_name: '歷史',
    contents: [],
  },
];

export default MainEventPageScreen;
