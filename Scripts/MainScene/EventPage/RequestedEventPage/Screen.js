import React, {Component} from 'react';
import {
  View,
  UIManager,
  StyleSheet,
  ScrollView,
  LayoutAnimation,
} from 'react-native';
import ExpandableItemComponent from '../ExpandableItemComponent';
import {getEventType} from '../../EventTypeGetter';
import {performRequestData} from '../../../Networking/NetworkRequest';

class RequestedEventPage extends Component {
  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      approvedEventData: [],
      rejectedEventData: [],
      pendingEventData: [],
      listDataSource: CONTENT,
    };
    this.requestData(10);
    this.requestData(20);
    this.requestData(30);
  }

  requestData(targetStatus) {
    performRequestData(
      global.apiUrl + 'getOwnRequest.shtml',
      {status: targetStatus},
      responseJson => {
        this.assignData(responseJson, targetStatus);
      },
    );
  }

  assignData(responseJson, targetStatus) {
    let eventRawData = responseJson[2];
    let eventList = [];

    for (let i = 0; i < eventRawData.length; i++) {
      let eventData = {
        id: eventRawData[i][5],
        eventType: getEventType([eventRawData[i][2]]),
        title: eventRawData[i][1],
        //startTime: eventRawData[i][6],
        startTime: '8th Sep, 3:00pm',
        //address: eventRawData[i][7],
        address: 'Pavilion',
        //stateAddress: eventRawData[i][8],
        stateAddress: 'Kuala Lumpur',
      };

      eventList.push(eventData);
    }

    let listDataSource = this.state.listDataSource;

    if (targetStatus === 10) {
      listDataSource[2].contents = eventList;
      this.setState({
        pendingEventData: eventList,
        listDataSource: listDataSource,
      });
    } else if (targetStatus === 20) {
      listDataSource[0].contents = eventList;
      this.setState({
        approvedEventData: eventList,
        listDataSource: listDataSource,
      });
    } else {
      listDataSource[1].contents = eventList;
      this.setState({
        rejectedEventData: eventList,
        listDataSource: listDataSource,
      });
    }
  }

  updateLayout = index => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...this.state.listDataSource];

    array[index]['isExpanded'] = !array[index]['isExpanded'];
    this.setState(() => {
      return {
        listDataSource: array,
      };
    });
  };

  onItemClicked(item) {
    this.props.navigation.navigate('EventDetails', item);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.state.listDataSource.map((item, key) => (
            <ExpandableItemComponent
              key={item.category_name}
              onClickFunction={this.updateLayout.bind(this, key)}
              onClickItemFunction={this.onItemClicked.bind(this)}
              item={item}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});

const CONTENT = [
  {
    isExpanded: true,
    category_name: '已通過',
    contents: [],
  },
  {
    isExpanded: true,
    category_name: '已被拒絕',
    contents: [],
  },
  {
    isExpanded: true,
    category_name: '待定中',
    contents: [],
  },
];

export default RequestedEventPage;
