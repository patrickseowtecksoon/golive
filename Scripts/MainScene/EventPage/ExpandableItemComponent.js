import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import EventTypeIconGetter from '../EventTypeIconGetter';
import EIcon from 'react-native-vector-icons/Entypo';

export default class ExpandableItemComponent extends Component {
  //Custom Component for the Expandable List
  constructor() {
    super();
    this.state = {
      layoutHeight: null,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.item.isExpanded) {
      this.setState(() => {
        return {
          layoutHeight: null,
        };
      });
    } else {
      this.setState(() => {
        return {
          layoutHeight: 0,
        };
      });
    }
  }
  renderArrow() {
    const result = [];
    if (this.state.layoutHeight == null) {
      result.push(
        <Icon name="angle-up" size={20} style={styles.HeaderArrow} />,
      );
    } else {
      result.push(
        <Icon name="angle-down" size={20} style={styles.HeaderArrow} />,
      );
    }

    return result;
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.props.onClickFunction}
          style={styles.HeaderSection}>
          <View style={styles.HeaderTextSection}>
            <Text style={styles.HeaderText}>
              {this.props.item.category_name +
                '(' +
                this.props.item.contents.length +
                ')'}
            </Text>
          </View>

          <View style={styles.HeaderArrowSection}>{this.renderArrow()}</View>
        </TouchableOpacity>
        <View
          style={{
            height: this.state.layoutHeight,
            overflow: 'hidden',
          }}>
          {this.props.item.contents.map((item, key) => (
            <View style={styles.Content}>
              <TouchableOpacity
                key={key}
                style={styles.ContentContainer}
                onPress={() => this.props.onClickItemFunction(item)}>
                <View style={{width: 20}}></View>
                <View style={styles.ContentEventImageSection}>
                  <View style={styles.ContentEventImage}>
                    <EventTypeIconGetter eventType={item.eventType} size={50} />
                  </View>
                </View>
                <View style={styles.ContentDescriptionSection}>
                  <Text style={styles.DescriptionTitleText}>{item.title}</Text>

                  <View style={styles.DescriptionContainer}>
                    <View style={styles.DescriptionIcon}>
                      <EIcon name="location-pin" size={18} color="gray" />
                    </View>

                    <Text style={styles.DescriptionLocationText}>
                      {item.address + ', ' + item.stateAddress}
                    </Text>
                  </View>
                  <View style={styles.DescriptionContainer}>
                    <View style={styles.DescriptionIcon}>
                      <EIcon name="clock" size={15} color="gray" />
                    </View>

                    <Text style={styles.DescriptionTimeText}>
                      {item.startTime}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  HeaderSection: {
    backgroundColor: '#F5FCFF',
    padding: 5,
    flexDirection: 'row',
  },
  HeaderTextSection: {
    flex: 0.9,
  },
  HeaderArrowSection: {
    flex: 0.1,
  },
  HeaderText: {
    fontSize: 16,
  },
  HeaderArrow: {
    alignSelf: 'center',
  },
  HeaderSeperator: {
    backgroundColor: 'grey',
    height: 1,
  },
  Content: {},
  ContentContainer: {
    flexDirection: 'row',
    borderBottomColor: 'rgba(150,150,150,1)',
    borderBottomWidth: 0.5,
  },
  ContentEventImageSection: {
    flex: 0.15,
    flexDirection: 'row',
  },
  ContentEventImage: {alignSelf: 'center'},
  ContentDescriptionSection: {flex: 0.85, height: 63},
  DescriptionTitleText: {
    fontSize: 15,
  },
  DescriptionTimeText: {
    fontSize: 12,
  },
  DescriptionLocationText: {
    fontSize: 12,
  },
  DescriptionContainer: {
    flexDirection: 'row',
  },
  DescriptionIcon: {
    width: 20,
  },
});
