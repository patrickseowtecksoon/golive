import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';

import {SliderBox} from 'react-native-image-slider-box';

const viewSize = Math.round(Dimensions.get('window').width) - 20;

class ProfileImages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        'https://images.unsplash.com/photo-1508138221679-760a23a2285b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=967&q=80',
        'https://chordify.net/pages/wp-content/uploads/2019/08/random-chiasso-1024x683.png',
        'https://scontent.fkul14-1.fna.fbcdn.net/v/t1.0-9/395950_10150489548128215_1483831042_n.jpg?_nc_cat=110&_nc_oc=AQme9N2n6l4xqNVxa8hkhw9pJvbFHSkdG7RQvz_eyxMyvVFZcCgZWDbf5dmNhH6ImRK0_21RTnmr91Vl91S4bDAv&_nc_ht=scontent.fkul14-1.fna&oh=3ba84193b9c4ec9a2acaccfac0f2a156&oe=5E529628',
        'https://images.unsplash.com/photo-1501426026826-31c667bdf23d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=676&q=80',
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <SliderBox
          images={this.state.images}
          parentWidth={viewSize}
          onCurrentImagePressed={index =>
            console.warn(`image ${index} pressed`)
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {width: viewSize},
});

export default ProfileImages;
