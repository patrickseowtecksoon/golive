import React, {Component} from 'react';
import {
  Text,
  View,
  Button,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import ProfileImages from './ProfileImages';
import {Divider} from 'react-native-elements';
import EventTypeIconGetter from '../../EventTypeIconGetter';

class DetailProfileScreen extends Component {
  render() {
    return (
      <View style={styles.MainContainer}>
        <View height={1}></View>
        <View style={styles.SegmentContainer}>
          <Text style={styles.BioLabel}>
            "When you feel like quitting, think about why you started"
          </Text>
        </View>

        <View stlye={{height: 8, color: 'white'}}></View>
        <Divider height={1} backgroundColor="grey" />
        <View style={styles.SegmentContainer}>
          <Text style={styles.InterestLabel}>興趣</Text>
          <View style={styles.InterestIconSection}>
            {this.renderInterestedIcons()}
          </View>
        </View>

        <View stlye={{height: 8, color: 'white'}}></View>
        <Divider height={1} backgroundColor="grey" />
        <View style={styles.SegmentContainer}>
          <Text style={styles.FeaturedLabel}>精選</Text>
          <Divider height={1} backgroundColor="grey" />
          <ProfileImages></ProfileImages>
        </View>
      </View>
    );
  }

  renderInterestedIcons() {
    const interestedIconSize = 40;
    const result = [];

    result.push(
      <EventTypeIconGetter
        eventType={availableEvents[0]}
        size={interestedIconSize}
        showLabel={true}
        labelStyle={styles.InterestIconLabel}
      />,
    );

    result.push(<View width={10}></View>);

    result.push(
      <EventTypeIconGetter
        eventType={availableEvents[2]}
        size={interestedIconSize}
        showLabel={true}
        labelStyle={styles.InterestIconLabel}
      />,
    );
    result.push(<View width={10}></View>);
    result.push(
      <EventTypeIconGetter
        eventType={availableEvents[3]}
        size={interestedIconSize}
        showLabel={true}
        labelStyle={styles.InterestIconLabel}
      />,
    );
    result.push(<View width={10}></View>);
    result.push(
      <EventTypeIconGetter
        eventType={availableEvents[5]}
        size={interestedIconSize}
        showLabel={true}
        labelStyle={styles.InterestIconLabel}
      />,
    );

    return result;
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    borderColor: 'rgba(0, 0, 0, 0)',

    backgroundColor: 'rgba(255,255,255, 1)',
    flex: 1,
  },
  SegmentContainer: {
    borderWidth: 10,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  NameLabel: {fontSize: 25, fontWeight: 'bold'},
  AgeLabel: {fontSize: 25},
  DetailContainer: {flex: 0, flexDirection: 'row'},
  DetailLabel: {width: 100},
  BioSection: {borderWidth: 1, borderColor: 'lightgray'},
  BioContainer: {
    borderWidth: 6,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  BioLabel: {color: '#595959', fontStyle: 'italic'},
  InterestSection: {
    borderWidth: 1,
    borderColor: 'lightgray',
    backgroundColor: 'white',
  },
  InterestContainer: {borderWidth: 5, borderColor: 'white'},
  InterestLabel: {fontSize: 14},
  InterestIconSection: {flexDirection: 'row'},
  InterestIcon: {height: 30},
  InterestIconLabel: {
    fontSize: 12,
    alignSelf: 'center',
    color: '#03506e',
    marginTop: -5,
  },
  FeaturedSection: {
    borderWidth: 1,
    borderColor: 'lightgray',
    backgroundColor: 'white',
  },
  FeaturedContainer: {borderWidth: 5, borderColor: 'white'},
  FeaturedLabel: {fontSize: 14},
});

export default DetailProfileScreen;
