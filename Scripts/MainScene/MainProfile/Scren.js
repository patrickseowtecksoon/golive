import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import OIcon from 'react-native-vector-icons/Octicons';
import ImagePicker from 'react-native-image-picker';

const imagePickerOptions = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class ProfileBaseScreen extends Component {
  CaptureProfilePhoto() {
    ImagePicker.showImagePicker(imagePickerOptions, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        //this.AddIcon(this.state.avatarSource, source);
      }
    });
  }

  render() {
    function RenderSetting(isSelf, navigation) {
      if (isSelf) {
        return (
          <View style={styles.SettingSection}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                navigation.navigate('ProfileSetting');
              }}>
              <Icon name="settings" size={25} style={styles.SettingButton} />
            </TouchableOpacity>
          </View>
        );
      }
    }

    function RenderEditProfileImage(isSelf) {
      if (isSelf) {
        return (
          <View style={styles.EditProfileImageSection}>
            <View style={styles.EditProfileImageContainer}>
              <Icon name="camera" size={16} color="#28A19C" />
            </View>
          </View>
        );
      }

      return null;
    }

    function RenderGender() {
      let tempHolder = 'm';

      if (tempHolder === 'm') {
        return <Icon name="gender-male" size={18}></Icon>;
      } else if (tempHolder === 'f') {
        return <Icon name="gender-female" size={20}></Icon>;
      }
    }

    return (
      <View style={styles.HeaderSection}>
        <View style={styles.HeaderHolder}>
          <TouchableWithoutFeedback
            onPress={() => {
              if (this.props.isSelf) this.CaptureProfilePhoto();
            }}>
            <View style={styles.ProfileImageSection}>
              <Image
                source={{
                  uri:
                    'https://scontent.fkul14-1.fna.fbcdn.net/v/t31.0-8/1009163_10201288928121039_346647159_o.jpg?_nc_cat=111&_nc_ohc=kNxzZxIqpCcAX-XEx7W&_nc_ht=scontent.fkul14-1.fna&oh=b4c77214a7b39005e526bdf70195b4ba&oe=5EDC2320',
                }}
                style={styles.ProfileIconImage}
              />
              {RenderEditProfileImage(this.props.isSelf)}
            </View>
          </TouchableWithoutFeedback>

          <View style={styles.DetailSection}>
            <View style={styles.DetailTopSection}>
              <Text style={styles.NameLabel}>{this.props.name}</Text>
              {RenderSetting(this.props.isSelf, this.props.navigation)}
            </View>
            <View style={styles.DetailMidSection}>
              <Text style={styles.AgeLabel}>{this.props.age}</Text>

              <OIcon name="primitive-dot" size={10} style={styles.DotDevider} />

              {RenderGender()}
            </View>

            <View style={styles.DetailBtmSection}>
              <Text style={styles.LocationLabel}>Kuala Lumpur</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  HeaderSection: {
    borderColor: 'rgba(0, 0, 0, 0)',
    borderWidth: 10,
    backgroundColor: 'white',
  },
  HeaderHolder: {flexDirection: 'row'},

  DetailSection: {
    flexDirection: 'column',
    flex: 1,
  },
  DetailTopSection: {
    flexDirection: 'row',
    borderWidth: 3,
    borderColor: 'white',
    width: '100%',
    flex: 1.5,
  },
  SettingSection: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
  },
  SettingButton: {
    color: '#278F9C',
    alignSelf: 'flex-end',
  },
  DetailMidSection: {
    flexDirection: 'row',
    borderLeftWidth: 5,
    borderColor: 'white',
    alignContent: 'flex-end',
    flex: 1,
  },
  DetailBtmSection: {
    flexDirection: 'row',
    borderLeftWidth: 5,
    borderColor: 'white',
    flex: 1,
  },
  ProfileImageSection: {
    height: 80,
    width: 80,
  },
  ProfileIconImage: {height: 80, width: 80, borderRadius: 40},
  EditProfileImageSection: {
    height: '100%',
    width: 30,
    position: 'absolute',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
  },
  EditProfileImageContainer: {
    height: 30,
    width: 30,
    borderRadius: 15,
    borderWidth: 3,
    borderColor: '#28A19C',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.75)',
  },

  NameLabel: {fontSize: 25, fontWeight: 'bold'},

  AgeLabel: {fontSize: 15, alignSelf: 'center', width: 18},

  LocationLabel: {fontSize: 15, alignSelf: 'center'},
  DotDevider: {
    alignSelf: 'center',
    paddingLeft: 3,
    paddingRight: 3,
  },
});

export default ProfileBaseScreen;
