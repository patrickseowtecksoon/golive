import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Button,
  TextInput,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import EventItem from '../../DetailEventItemGetter';
import MultipleImagePicker from '../../Utils/ImagePicker/MultipleImagePicker';
import ImagePicker from 'react-native-image-picker';
import IconGetter from '../../EventTypeIconGetter';

const imagePickerOptions = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class EventScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'ChunKit Wong',
      profile: {
        uri:
          'https://scontent.fkul14-1.fna.fbcdn.net/v/t31.0-8/1009163_10201288928121039_346647159_o.jpg?_nc_cat=111&_nc_ohc=kNxzZxIqpCcAX-XEx7W&_nc_ht=scontent.fkul14-1.fna&oh=b4c77214a7b39005e526bdf70195b4ba&oe=5EDC2320',
      },
      bio: 'When you feel like quitting, think about why you started',
      favouriteEvent: [0, 1, 2, 3, 4],
      uploadedPictureCount: 0,
      address: 'Kuala Lumpur',
    };
  }

  get GetFavouriteFlatListData() {
    let favouriteEvent = this.state.favouriteEvent;
    let result = [];

    for (let i = 0; i < global.availableEvents.length; i++) {
      result.push({
        id: i,
        eventType: global.availableEvents[i],
        selected: favouriteEvent.includes(i) ? true : false,
      });
    }
    return result;
  }

  setFavouriteOptionSelected(targetEventTypeId) {
    let newList = this.state.favouriteEvent;
    if (newList.includes(targetEventTypeId)) {
      newList.splice(newList.indexOf(targetEventTypeId), 1);
    } else {
      newList.push(targetEventTypeId);
    }

    this.setState({favouriteEvent: newList});
  }

  render() {
    const navigationRef = this.props.navigation;
    return (
      <ScrollView style={styles.MainContainer}>
        <View style={styles.OptionLabelContainer}>
          <Text style={styles.OptionText}>昵稱</Text>
        </View>
        <View style={styles.OptionContentContainer}>
          <TextInput
            style={styles.EventTitleTextInput}
            onChangeText={text => this.setState({eventTitle: text})}
            value={this.state.name}
          />
        </View>

        <View style={styles.OptionLabelContainer}>
          <Text style={styles.OptionText}>所在地區</Text>
        </View>
        <View style={styles.OptionContentContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              navigationRef.navigate('PlacePick', {
                locationAcquiredCallback: locationData => {
                  this.setState({
                    address:
                      locationData.addressLevel1 +
                      ', ' +
                      locationData.addressLevel2,
                  });
                },
                searchLevel: '(regions)',
              });
            }}>
            <View style={styles.LocationTextSecttion}>
              <Text style={styles.LocationText}>{this.state.address}</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.OptionLabelContainer}>
          <Text style={styles.OptionText}>簡介</Text>
        </View>
        <View style={styles.OptionContentContainer}>
          <View style={styles.BioTextContainer}>
            <TextInput
              multiline={true}
              numberOfLines={2}
              maxLength={130}
              value={this.state.bio}
              onChangeText={value => this.setState({bio: value})}
              style={styles.BioTextField}
            />
            <Text style={styles.BioRemainingText}>
              {this.state.bio.length}/130
            </Text>
          </View>
        </View>

        <View style={styles.OptionLabelContainer}>
          <Text style={styles.OptionText}>興趣</Text>
        </View>
        <View style={styles.OptionContentContainer}>
          <FlatList
            data={this.GetFavouriteFlatListData}
            style={styles.FavouriteFlatList}
            numColumns={8}
            renderItem={({item, index}) => (
              <View
                style={[
                  styles.FavouriteItem,
                  {opacity: item.selected ? 1 : 0.25},
                ]}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    {
                      this.setFavouriteOptionSelected(item.id);
                    }
                  }}>
                  <View>
                    <IconGetter
                      eventType={item.eventType}
                      size={30}
                      showLabel={true}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
            )}
            keyExtractor={item => item.id}
          />
        </View>

        <View style={styles.OptionLabelContainer}>
          <Text style={styles.OptionText}>
            {'圖集(' + (this.state.uploadedPictureCount + '/4)')}
          </Text>
        </View>
        <View style={{height: 7}}></View>
        <View style={styles.OptionContentContainer}>
          <MultipleImagePicker
            maxCount={4}
            countUpdatedCallback={param => {
              this.setState({uploadedPictureCount: param});
            }}></MultipleImagePicker>
        </View>

        <View style={{height: 7}}></View>
        <View style={styles.Devider} />
        <View style={{height: 12}}></View>
        <View style={styles.OptionContentContainer}>
          <TouchableOpacity
            onPress={() => {
              navigationRef.navigate('WebView', {link: 'http://google.com'});
            }}>
            <Text style={styles.HyperlinkText}>User agreement</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigationRef.navigate('WebView', {link: 'http://google.com'});
            }}>
            <Text style={styles.HyperlinkText}>Privacy policy</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigationRef.navigate('WebView', {link: 'http://google.com'});
            }}>
            <Text style={styles.HyperlinkText}>Term & condition</Text>
          </TouchableOpacity>
          <View style={{height: 20}}></View>
          <Button
            title="確認"
            onPress={() => {
              navigationRef.goBack();
            }}
            color="#28A19C"
          />
          <View style={{height: 7}}></View>
          <Button
            title="登出"
            onPress={() => {
              navigationRef.navigate('Login');
            }}
            color="#28A19C"
          />
        </View>

        <View style={{height: 20}}></View>
      </ScrollView>
    );

    function RenderContent(itemParams) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          style={styles.ContentItem}
          onPress={() => {
            navigationRef.navigate('EventDetails', itemParams.itemParams.item);
          }}>
          <EventItem itemData={itemParams} />
        </TouchableOpacity>
      );
    }
  }
}

const styles = StyleSheet.create({
  MainContainer: {},
  Devider: {
    height: 2,
    backgroundColor: '#c9c9c9',
  },
  OptionLabelContainer: {
    backgroundColor: '#d6d6d6',
    height: 30,
    justifyContent: 'center',
    borderLeftWidth: 10,
    borderColor: '#d6d6d6',
  },
  OptionText: {
    fontSize: 17,
    color: '#03506e',
  },
  OptionContentContainer: {
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderColor: 'rgba(0, 0, 0, 0)',
  },
  LocationTextSecttion: {
    height: 40,
    justifyContent: 'center',
  },
  LocationText: {
    paddingLeft: 6,
    fontSize: 13,
    textAlign: 'left',
    textAlignVertical: 'center',
  },
  BioTextContainer: {},
  BioTextField: {
    textAlignVertical: 'top',
  },
  BioRemainingText: {
    alignSelf: 'flex-end',
    padding: 3,
    color: '#c9c9c9',
  },
  FavouriteFlatList: {},
  FavouriteItem: {
    padding: 7,
  },
  ConfirmButton: {
    width: 100,
    height: 35,
    borderRadius: 10,
    backgroundColor: '#03506e',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  ConfirmButtonText: {
    fontSize: 15,
    alignSelf: 'center',
    color: 'white',
  },
  LogoutButton: {
    width: 100,
    height: 35,
    borderRadius: 10,
    backgroundColor: '#03506e',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  LogoutButtonText: {
    fontSize: 50,
    alignSelf: 'center',
    color: 'white',
  },
  HyperlinkText: {
    fontSize: 12,
    color: 'blue',
    textDecorationLine: 'underline',
  },
});

export default EventScreen;
