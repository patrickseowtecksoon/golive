import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, Dimensions} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import EIcon from 'react-native-vector-icons/Entypo';
import IconGetter from './EventTypeIconGetter';

const itemWidth = Math.round(Dimensions.get('window').width);
const itemHeight = Math.round(Dimensions.get('window').width) * 0.56;

class GeneralContent extends Component {
  render() {
    return (
      <View style={styles.MainContainer}>
        <View style={styles.ItemContainer}>
          <Image
            source={{uri: this.props.itemData.itemParams.item.imgLink}}
            style={{width: '100%', height: '100%'}}
          />
        </View>
        <View style={styles.TextSection}>
          <View style={styles.TitleSection}>
            <View style={styles.EventTypeIcon}>
              <IconGetter
                eventType={this.props.itemData.itemParams.item.eventType}
                size={20}
                iconColor="black"
              />
            </View>
            <View style={{width: 5}} />
            <Text style={styles.TitleText}>
              {this.props.itemData.itemParams.item.title}
            </Text>
          </View>

          <View style={styles.SubTextSection}>
            <EIcon name="clock" size={17} style={styles.SubTextImage} />
            <Text style={styles.TimeText}>
              {this.props.itemData.itemParams.item.time}
            </Text>
          </View>
          <View style={styles.SubTextSection}>
            <EIcon name="location-pin" size={22} style={styles.SubTextImage} />
            <Text style={styles.LocationText}>
              {this.props.itemData.itemParams.item.location}
            </Text>
          </View>
          <View style={styles.SubTextSection}>
            <FAIcon name="user" size={18} style={styles.SubTextImage} />
            <Text style={styles.HostText}>擧辦人</Text>
            <Image
              source={{
                uri: this.props.itemData.itemParams.item.hostInfo.imgLink,
              }}
              style={styles.HostProfileImage}
            />
            <Text style={styles.HostText}>
              {this.props.itemData.itemParams.item.hostInfo.name}
            </Text>
          </View>
          <View style={{height: 10}} />
        </View>
      </View>
    );
  }

  get stars() {
    const starsNumber = 5;
    const color = '#FEE12B';
    const starElements = [];
    for (let i = 0; i < 5; i++) {
      starElements.push(
        <FAIcon
          name="star"
          size={15}
          color={starsNumber > i ? color : '#FEE12B'}
          style={styles.star}
        />,
      );
    }
    return starElements;
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    paddingBottom: 0,
  },

  ItemContainer: {
    width: itemWidth,
    height: itemHeight,
    flexDirection: 'row',
  },
  TextSection: {
    borderWidth: 8,
    borderColor: 'rgba(245,245,245,1)',
    backgroundColor: 'rgba(245,245,245,1)',
  },
  TitleSection: {flexDirection: 'row'},
  EventTypeIcon: {
    alignSelf: 'center',
  },
  TitleText: {
    fontSize: 18,
    color: 'black',
  },
  SubTextSection: {
    flexDirection: 'row',
  },
  SubTextImage: {
    width: 20,
    justifyContent: 'center',
    textAlign: 'center',
    alignSelf: 'center',
  },
  TimeText: {
    fontSize: 12,
    color: 'rgba(50,50,50,1)',
    alignSelf: 'center',
  },
  LocationText: {
    fontSize: 12,
    color: 'rgba(30,30,30,1)',
    alignSelf: 'center',
  },
  HostText: {
    fontSize: 12,
    color: 'rgba(30,30,30,1)',
    alignSelf: 'center',
  },
  HostProfileImage: {
    alignSelf: 'center',
    height: 24,
    width: 24,
    borderRadius: 12,
  },
});

export default GeneralContent;
