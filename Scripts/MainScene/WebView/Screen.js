import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';

class EventScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <WebView
        source={{uri: this.props.route.params.link}}
        style={{marginTop: 20}}
      />
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {},
});

export default EventScreen;
