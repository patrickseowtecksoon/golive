import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  FlatList,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import EIcon from 'react-native-vector-icons/Entypo';
import OIcon from 'react-native-vector-icons/Octicons';
import EventTypeIconGetter from '../EventTypeIconGetter';
import {getEventString} from '../EventTypeGetter';
import ParticipantList from './ParticipantPopUp/PopUp';
import ApprovalList from './ApprovalListPopUp/PopUp';
import {performRequestData} from '../../../Scripts/Networking/NetworkRequest';

class EventDetailScreen extends Component {
  constructor(props) {
    super(props);
    let defaultParams = props.route.params;

    this.state = {
      isHost: false,
      id: defaultParams.id,
      title: defaultParams.title,
      eventType: defaultParams.eventType,
      eventImg: defaultParams.imgLink,
      host:
        defaultParams.hostInfo != undefined
          ? {
              id: -1,
              name: defaultParams.hostInfo.name,
              img: defaultParams.hostInfo.imgLink,
            }
          : null,
      caption: 'this is caption...',
      startTime: '',
      endTime: '',
      location: '',
      participants: [],
      pendingApprovalUsers: [],
      eventPhotos: [],
      status: -1,
    };

    this.requestData(this.state.id);
  }

  requestData(eventId) {
    performRequestData(
      global.apiUrl + 'getEventDetail.shtml',
      {eventToken: eventId},
      responseJson => {
        this.assignData(responseJson);
      },
    );
  }

  assignData(responseJson) {
    this.setState({
      //eventImg: responseJson[2][15],
      eventImg:
        'https://d3hne3c382ip58.cloudfront.net/files/uploads/bookmundi/resized/cmsfeatured/hiking-trails-indonesia-1523345692-785X440.jpg',
      caption: responseJson[2][1],
      host: {
        id: responseJson[2][18],
        name: responseJson[2][16],
        //img: responseJson[2][17]
        img:
          'https://image.shutterstock.com/image-photo/handsome-fit-young-man-countryside-260nw-276450281.jpg',
      },
      startTime: responseJson[2][7],
      endTime: responseJson[2][8],
      address: responseJson[2][12] + ', ' + responseJson[2][14],
      status: responseJson[2][10],
      isHost: global.userToken === responseJson[2][18],
    });

    if (this.state.isHost) {
      this.requestApprovalList(this.state.id);
    }
  }

  requestApprovalList(eventId) {
    performRequestData(
      global.apiUrl + 'getRequestList.shtml',
      {eventToken: eventId},
      responseJson => {
        this.assignApprovalListData(responseJson);
      },
    );
  }

  assignApprovalListData(responseJson) {
    let rawApprovalResponse = responseJson[2];
    let approvalData = [];

    for (let i = 0; i < rawApprovalResponse.length; i++) {
      approvalData.push({
        id: rawApprovalResponse[i][0],
        name: rawApprovalResponse[i][1],
        //img: rawApprovalResponse[i][2],
        img:
          'https://image.shutterstock.com/image-photo/handsome-fit-young-man-countryside-260nw-276450281.jpg',
      });
    }

    this.setState({pendingApprovalUsers: approvalData});
  }

  getEventIcon(eventType) {
    return (
      <EventTypeIconGetter
        eventType={eventType}
        size={20}
        iconColor="black"></EventTypeIconGetter>
    );
  }

  getEventText(eventType) {
    return (
      <Text style={styles.EventTypeText}>{getEventString(eventType)}</Text>
    );
  }

  render() {
    const {navigation} = this.props;
    var data = this.state;

    function DoJoinerSection(participants, pendingApprovalUsers, navigation) {
      if (participants.length === 0 && pendingApprovalUsers.length === 0)
        return;

      let result = [];
      result.push(
        <View style={{height: 0.7, backgroundColor: 'rgba(150,150,150,1)'}} />,
      );
      result.push(
        <View style={styles.SegmentContainer}>
          {DoParticipantsSection(participants, navigation)}
          <View style={{height: 5}} />
          {DoPendingApprovalSection(pendingApprovalUsers, navigation)}
        </View>,
      );
      return result;
    }

    function DoParticipantsSection(participants, navigation) {
      let result = [];
      if (participants !== undefined && participants.length !== 0)
        result.push(
          <ParticipantList data={participants} navigation={navigation} />,
        );

      return result;
    }

    function DoPendingApprovalSection(pendingApprovalUsers, navigation) {
      let result = [];
      if (
        pendingApprovalUsers !== undefined &&
        pendingApprovalUsers.length !== 0
      )
        result.push(
          <ApprovalList data={pendingApprovalUsers} navigation={navigation} />,
        );

      return result;
    }

    function DoPhotosSection() {
      let eventPhotosCount =
        data.eventPhotos !== undefined ? data.eventPhotos.length : 0;
      if (eventPhotosCount === 0) return;

      let result = [];
      result.push(
        <View style={{height: 0.7, backgroundColor: 'rgba(150,150,150,1)'}} />,
      );
      result.push(
        <View style={styles.SegmentContainer}>
          <View style={styles.PhotosSection}>
            <Text style={styles.PhotosCountLabel}>
              相冊 ({eventPhotosCount})
            </Text>
            <FlatList
              data={data.eventPhotos}
              renderItem={({item}) => (
                <View style={styles.PhotosSectionPhotoContainer}>
                  <Image
                    source={{uri: item.img}}
                    style={styles.PhotosSectionPhotoImage}
                  />
                </View>
              )}
              keyExtractor={item => item.id}
              horizontal={true}
            />
          </View>
        </View>,
      );
      return result;
    }

    function DoStatusSection(status) {
      let result = [];

      if (status === 10) {
        result.push(<OIcon name="primitive-dot" size={20} color="green" />);
        result.push(<Text style={styles.EventStatusText}>進行中</Text>);
      } else if (status === 20) {
        result.push(<OIcon name="primitive-dot" size={20} color="orange" />);
        result.push(<Text style={styles.EventStatusText}>未開始</Text>);
      } else if (status === 30) {
        result.push(<OIcon name="primitive-dot" size={20} color="grey" />);
        result.push(<Text style={styles.EventStatusText}>已結束</Text>);
      }

      return result;
    }

    function DoHostSection(hostData, navigation) {
      if (hostData === null) {
        return;
      } else {
        return (
          <View style={styles.SegmentItem}>
            <View style={{height: 5}} />
            <View style={styles.SegmentItemIcon}>
              <View>
                <FAIcon name="user" size={25} style={styles.HostIcon} />
              </View>
            </View>
            <View>
              <View style={styles.HostSection}>
                <Text style={styles.HostText}>擧辦人: </Text>
                <TouchableWithoutFeedback
                  onPress={() => {
                    if (hostData.id === -1) return;

                    navigation.navigate('ViewProfile', {
                      id: hostData.id,
                      method: 'ViewHost',
                    });
                  }}>
                  <Image
                    source={{
                      uri: hostData.img,
                    }}
                    style={styles.HostProfileImage}
                  />
                </TouchableWithoutFeedback>

                <Text style={styles.HostText}>{' ' + hostData.name}</Text>
              </View>
            </View>
          </View>
        );
      }
    }

    return (
      <ScrollView style={styles.MainContainer}>
        <View style={styles.ImageSection}>
          <Image
            source={{
              uri: data.eventImg,
            }}
            style={styles.EventImage}
          />
        </View>
        <View style={styles.SegmentContainer}>
          <View style={styles.EventtitleSection}>
            <View style={styles.EventTitleTextSection}>
              <Text style={styles.EventTitle}>{data.title}</Text>
            </View>
            <View style={styles.EventStatusSection}>
              <View style={styles.EventStatusContainer}>
                {DoStatusSection(data.status)}
              </View>
            </View>
          </View>
          <View style={styles.SegmentItem}>
            <View style={styles.SegmentItemIcon}>
              {this.getEventIcon(data.eventType)}
            </View>
            <View>{this.getEventText(data.eventType)}</View>
          </View>
          <View style={{height: 5}} />
          <View style={styles.SegmentItem}>
            <View style={styles.SegmentItemIcon}>
              <View>
                <EIcon name="clock" size={17} />
              </View>
            </View>
            <View>
              <View style={styles.TimeTextSection}>
                <Text style={styles.TimeText}>開始：</Text>
                <Text style={styles.TimeText}>{data.startTime}</Text>
              </View>

              <View style={styles.TimeTextSection}>
                <Text style={styles.TimeText}>結束：</Text>
                <Text style={styles.TimeText}>{data.endTime}</Text>
              </View>
            </View>
          </View>
          <View style={{height: 5}} />
          <View style={styles.SegmentItem}>
            <View style={styles.SegmentItemIcon}>
              <View>
                <EIcon name="location-pin" size={23} />
              </View>
            </View>
            <View>
              <Text style={styles.locationText}>{data.address}</Text>
            </View>
          </View>
          <View style={{height: 5}} />
          <View style={styles.SegmentItem}>
            <View style={styles.SegmentItemIcon}>
              <View>
                <FAIcon name="edit" size={20} style={styles.HostIcon} />
              </View>
            </View>
            <View>
              <Text style={styles.RemarkText}>{data.caption}</Text>
            </View>
          </View>
          {DoHostSection(data.host, navigation)}
        </View>
        {DoJoinerSection(
          data.participants,
          data.pendingApprovalUsers,
          this.props.navigation,
        )}
        {DoPhotosSection()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    backgroundColor: 'white',
  },

  ImageSection: {
    height: Dimensions.get('window').width * 0.65,
    width: Dimensions.get('window').width,
    flexDirection: 'row',
  },
  EventImage: {
    alignSelf: 'center',
    height: '100%',
    width: '100%',
  },

  SegmentContainer: {
    backgroundColor: 'white',
    borderWidth: 8,
    borderColor: 'white',
  },
  SegmentItem: {flexDirection: 'row', borderLeftWidth: 5, borderColor: 'white'},
  SegmentItemIcon: {
    width: 30,
    alignSelf: 'center',
    alignItems: 'center',
  },
  EventtitleSection: {flexDirection: 'row', alignItems: 'center'},
  EventTitleTextSection: {
    flex: 0.8,
  },
  EventTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#278F9C',
  },
  EventStatusSection: {flex: 0.2},
  EventStatusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
  EventStatusText: {
    fontSize: 13,
  },
  EventTypeText: {
    fontSize: 15,
  },

  TimeTextSection: {
    flexDirection: 'row',
  },

  TimeText: {
    fontSize: 15,
  },

  LocationText: {
    fontSize: 15,
  },

  RemarkText: {
    fontSize: 15,
  },

  HostSection: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  HostIcon: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    textAlign: 'center',
  },
  HostText: {
    fontSize: 15,
    alignSelf: 'center',
  },
  HostProfileImage: {
    alignSelf: 'center',
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
  },
  PhotosSection: {},
  PhotosCountLabel: {
    fontSize: 15,
  },
  PhotosSectionPhotoContainer: {
    padding: 3,
  },
  PhotosSectionPhotoImage: {height: 80, aspectRatio: 1.3},
});

export default EventDetailScreen;
