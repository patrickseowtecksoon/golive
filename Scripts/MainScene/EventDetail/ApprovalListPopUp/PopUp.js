import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PopUpContent from './PopUpContent';

const profileIconMaxDisplayCount = 6;

class ApprovalListPopUp extends Component {
  state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    const data = this.props.data;

    function ClampParticipantData(participants) {
      let participantsCount =
        participants !== undefined ? participants.length : 0;
      let clampedCount = participantsCount - profileIconMaxDisplayCount;
      const result = [];

      for (let i = 0; i < profileIconMaxDisplayCount; i++) {
        if (i < participantsCount) result.push(participants[i]);
      }
      return {clampedCount: clampedCount, result: result};
    }

    function DoParticipantIconSection() {
      let result = [];
      let clampedProps = ClampParticipantData(data);

      result.push(
        <View style={styles.ParticipantProfileIconSection}>
          <FlatList
            data={clampedProps.result}
            renderItem={({item}) => (
              <RenderParticipantItem imgLink={item.img} />
            )}
            keyExtractor={item => item.id}
            horizontal={true}
          />
        </View>,
      );

      if (clampedProps.clampedCount > 0) {
        result.push(
          <TouchableWithoutFeedback
            style={styles.ParticipantProfileIconReadMoreContainer}>
            <View>
              <Text style={styles.ParticipantProfileIconReadMoreText}>
                + {clampedProps.clampedCount.toString()} more
              </Text>
              <Icon name="more" size={20} />
            </View>
          </TouchableWithoutFeedback>,
        );
      }

      return result;
    }

    function RenderParticipantItem({imgLink}) {
      return (
        <View style={styles.ParticipantProfileIcon}>
          <Image
            source={{uri: imgLink}}
            style={styles.ParticipantProfileIconImage}
          />
        </View>
      );
    }

    return (
      <View style={styles.contentContainer}>
        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}
          style={styles.touchableHighlight}
          underlayColor={'#f1f1f1'}>
          <View>
            <Text style={styles.ParticipantCountLabel}>
              待確認 ({data.length})
            </Text>
            <View style={styles.ParticipantProfileSection}>
              {DoParticipantIconSection()}
            </View>
          </View>
        </TouchableHighlight>

        <Modal
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(false);
          }}
          animationType="fade"
          transparent={true}>
          <PopUpContent
            data={data}
            navigation={this.props.navigation}
            changeModalVisibility={this.setModalVisible.bind(this)}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {},
  touchableHighlight: {
    backgroundColor: 'white',
    alignItems: 'stretch',
  },
  ParticipantCountLabel: {
    fontSize: 15,
  },
  ParticipantProfileSection: {
    flexDirection: 'row',
  },
  ParticipantProfileIconSection: {
    flexDirection: 'column',
  },
  ParticipantProfileIcon: {
    padding: 2,
  },
  ParticipantProfileIconImage: {
    height: 35,
    width: 35,
    borderRadius: 35 / 2,
  },
  ParticipantProfileIconReadMoreContainer: {
    alignSelf: 'center',
  },
  ParticipantProfileIconReadMoreText: {
    fontSize: 10,
    fontStyle: 'italic',
  },
});

export default ApprovalListPopUp;
