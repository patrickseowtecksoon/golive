import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FIcon from 'react-native-vector-icons/FontAwesome';

class PopUpContent extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    Dimensions.addEventListener('change', e => {
      this.setState(e.window);
    });
  }

  closeModal() {
    this.props.changeModalVisibility(false);
  }

  render() {
    const navRef = this.props.navigation;

    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          this.props.changeModalVisibility(false);
        }}
        style={styles.ContentContainer}>
        <TouchableOpacity activeOpacity={1}>
          <View style={[styles.Modal]}>
            <View style={styles.HeaderSection}>
              <Text style={styles.HeaderText}>待確認</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.CloseButton}
                onPress={() => {
                  this.props.changeModalVisibility(false);
                }}>
                <Icon name="close" size={30} />
              </TouchableOpacity>
            </View>

            <View style={styles.Devider} />
            <View></View>
            <View style={styles.ContentListSection}>
              <FlatList
                data={this.props.data}
                style={styles.ContentListContainer}
                renderItem={({item, index}) => {
                  return (
                    <View style={styles.ContentItem}>
                      <TouchableOpacity
                        activeOpacity={1.0}
                        onPress={() => {
                          this.closeModal();
                          navRef.navigate('ViewProfile', {
                            id: item.id,
                            method: 'PopUp-ApprovalList',
                          });
                        }}>
                        <Image
                          source={{uri: item.img}}
                          style={styles.ParticipantProfileIcon}
                        />
                      </TouchableOpacity>

                      <View style={styles.ItemDetailSection}>
                        <TouchableOpacity
                          style={{alignSelf: 'center'}}
                          activeOpacity={1.0}
                          onPress={() => {
                            this.closeModal();
                            navRef.navigate('ViewProfile', {
                              id: item.id,
                              method: 'PopUp-ApprovalList',
                            });
                          }}>
                          <Text style={styles.ParticipantNameLabel}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>

                        <View style={styles.ParticipantApproveSection}>
                          <TouchableOpacity>
                            <View style={styles.ParticipantApproveButton}>
                              <FIcon name="check" size={24} color={'#28A19C'} />
                            </View>
                          </TouchableOpacity>

                          <TouchableOpacity>
                            <View style={styles.ParticipantApproveButton}>
                              <FIcon name="close" size={24} color={'#28A19C'} />
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  );
                }}
                keyExtractor={item => item.id}
                alignItems="center"
              />
            </View>
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

  get getAvailableFilterData() {
    return this.state.filterEventData;
  }
}

const styles = StyleSheet.create({
  ContentContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  Modal: {
    borderColor: 'white',
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 15,
    width: 295,
  },
  HeaderSection: {
    width: '100%',
    height: 30,
    alignItems: 'center',
  },
  HeaderText: {
    fontSize: 19,
  },
  CloseButton: {
    alignSelf: 'flex-end',
    position: 'absolute',
  },
  Devider: {
    width: '105%',
    height: 1,
    backgroundColor: '#c9c9c9',
  },

  ContentListSection: {
    width: 285,
    height: 400,
  },
  ContentListContainer: {
    width: '100%',
  },
  ContentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 55,
    width: 270,
    borderBottomWidth: 0.5,
    borderColor: '#c9c9c9',
  },
  ItemDetailSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',

    flex: 1,
  },
  ParticipantNameLabel: {
    fontSize: 18,
    paddingLeft: 10,
    alignSelf: 'center',
  },
  ParticipantProfileIcon: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
  },
  ParticipantApproveSection: {alignSelf: 'center', flexDirection: 'row'},
  ParticipantApproveButton: {
    width: 55,
    height: 35,

    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default PopUpContent;
