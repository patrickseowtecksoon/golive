import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  FlatList,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class PopUpContent extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    Dimensions.addEventListener('change', e => {
      this.setState(e.window);
    });
  }

  closeModal() {
    this.props.changeModalVisibility(false);
  }

  render() {
    const navRef = this.props.navigation;

    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          this.props.changeModalVisibility(false);
        }}
        style={styles.ContentContainer}>
        <TouchableOpacity activeOpacity={1}>
          <View style={[styles.Modal]}>
            <View style={styles.HeaderSection}>
              <Text style={styles.HeaderText}>參與者</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.CloseButton}
                onPress={() => {
                  this.props.changeModalVisibility(false);
                }}>
                <Icon name="close" size={30} />
              </TouchableOpacity>
            </View>

            <View style={styles.Devider} />
            <View></View>
            <View style={styles.ContentListSection}>
              <FlatList
                data={this.props.data}
                style={styles.ContentListContainer}
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      activeOpacity={1}
                      style={styles.ContentItem}
                      onPress={() => {
                        this.closeModal();
                        navRef.navigate('ViewProfile', {
                          id: item.id,
                          method: 'PopUp-ParticipantList',
                        });
                      }}>
                      <Image
                        source={{uri: item.img}}
                        style={styles.ParticipantProfileIcon}
                      />
                      <Text style={styles.ParticipantNameLabel}>
                        {item.name}
                      </Text>
                    </TouchableOpacity>
                  );
                }}
                keyExtractor={item => item.id}
                alignItems="center"
              />
            </View>
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

  get getAvailableFilterData() {
    return this.state.filterEventData;
  }
}

const styles = StyleSheet.create({
  ContentContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  Modal: {
    borderColor: 'white',
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 15,
    width: 295,
  },
  HeaderSection: {
    width: '100%',
    height: 30,
    alignItems: 'center',
  },
  HeaderText: {
    fontSize: 19,
  },
  CloseButton: {
    alignSelf: 'flex-end',
    position: 'absolute',
  },
  Devider: {
    width: '105%',
    height: 1,
    backgroundColor: '#c9c9c9',
  },

  ContentListSection: {
    width: 285,
    height: 400,
  },
  ContentListContainer: {
    width: '100%',
  },
  ContentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 55,
    width: 270,
    borderBottomWidth: 0.5,
    borderColor: '#c9c9c9',
  },
  ParticipantNameLabel: {
    fontSize: 18,
    paddingLeft: 10,
  },
  ParticipantProfileIcon: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
  },
});

export default PopUpContent;
