import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Image,
} from 'react-native';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    global.userToken = 'usertoken1';
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <Image source={require('../../Assets/Logo.png')} style={styles.Icon} />

        <Text style={styles.TitleText}>Go Live!</Text>
        <View style={{height: 10}} />
        {/* <TextInput
          style={styles.InputTextField}
          placeholder="用戶名"></TextInput>
        <View style={{height: 5}} />
        <TextInput style={styles.InputTextField} placeholder="密碼"></TextInput>
        <View style={{height: 5}} />
        <TouchableOpacity
          style={styles.LoginButton}
          onPress={() => {
            this.props.navigation.navigate('Main');
          }}
          activeOpacity={0.8}>
          <Text style={styles.LoginButtonText}>登錄</Text>
        </TouchableOpacity>
        <View style={{height: 5}} /> */}
        <TouchableOpacity
          style={styles.LoginButton}
          onPress={() => {
            this.props.navigation.navigate('Main');
          }}
          activeOpacity={0.8}>
          <Text style={styles.LoginButtonText}>FACEBOOK 登錄</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
  },
  Icon: {width: 140, height: 140},
  TitleText: {
    fontSize: 30,
    color: '#212121',
    fontWeight: 'bold',
  },

  InputTextField: {
    width: 220,
    height: 40,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#8f8f8f',
    backgroundColor: 'white',
  },
  LoginButton: {
    width: 220,
    height: 40,
    backgroundColor: '#28A19C',
    borderRadius: 10,
    justifyContent: 'center',
  },
  LoginButtonText: {
    fontSize: 15,
    alignSelf: 'center',
    color: '#002627',
  },
});

export default LoginScreen;
