import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RootNavigation from './Scripts/Navigation/RootNavigation';
import {getAvailableEvents} from './Scripts/MainScene/EventTypeGetter';

export default class App extends Component {
  constructor() {
    super();
    global.availableEvents = getAvailableEvents();
    global.apiUrl = 'http://appapi.goliveapi.xyz:8080/goliveapi/ajax/golive/';
  }

  render() {
    return (
      <NavigationContainer>
        <RootNavigation />
      </NavigationContainer>
    );
  }
}
